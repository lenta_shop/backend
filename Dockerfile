FROM python:3.10.8 AS venv

RUN curl -sSL https://install.python-poetry.org | python3.10 -
ENV PATH="/root/.local/bin:$PATH"

WORKDIR /app
COPY poetry.lock pyproject.toml ./

RUN python -m venv --copies /deps/venv \
    && . /deps/venv/bin/activate && poetry install --only main

FROM python:3.10.8-slim
ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1
COPY --from=venv /deps/venv /deps/venv/
ENV PATH /deps/venv/bin:$PATH
EXPOSE 8000
WORKDIR /app
COPY . ./
CMD ["python", "-m", "gunicorn", \
	"--bind", ":8000", \
	"--workers", "3", \
	"--capture-output", \
	"--log-level", "error", \
	"lenta.wsgi:application"]
