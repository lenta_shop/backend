from django.db import models
from django.db.models.fields import CharField
from django.db.models.fields.related import ForeignKey
import uuid


class TelegramUser(models.Model):
    
    NO_USERNAME = "_NO_USERNAME"

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"

    tg_id = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=32, default=NO_USERNAME)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name if self.name != self.NO_USERNAME else str(self.tg_id)


class Category(models.Model):
    parent = models.ForeignKey(
        "self", related_name="children",
        on_delete=models.CASCADE, blank = True, null=True)
    title = models.CharField(max_length=128) 
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ("title", "parent",)    
        verbose_name = "Категория"
        verbose_name_plural = "Категории"     

    def __str__(self):
        if self.parent:
            return f"{self.parent.title} > {self.title}"
        return self.title


class Order(models.Model):

    class Meta:
        verbose_name = "Заказ"
        verbose_name_plural = "Заказы"  

    class Status(models.TextChoices):
        PENDING = ("pending", "Ожидает оплаты")
        PAID = ("paid", "Оплачен")

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    user = ForeignKey(TelegramUser, related_name="orders", on_delete=models.SET_NULL, null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    items = models.JSONField()
    status = models.CharField(
        verbose_name="Статус заказа",
        max_length=7,
        choices=Status.choices,
        default=Status.PENDING,
    )
    total = models.PositiveIntegerField(verbose_name="Сумма")
    
    def __str__(self):
        return str(self.id)


class Product(models.Model):
    class Meta:
        verbose_name = "Продукт"
        verbose_name_plural = "Продукты"     

    title = models.CharField(max_length=128) 
    description = models.CharField(max_length=512)
    category = models.ForeignKey(Category, related_name="products", on_delete=models.SET_NULL, null=True)
    price = models.IntegerField()
    stock = models.IntegerField(verbose_name="Доступно для покупки")
    tg_photo_id = models.CharField(max_length=258, null=True, blank=True)

    def __str__(self):
        return self.title


class FAQ(models.Model):
    class Meta:
        verbose_name = "FAQ"
        verbose_name_plural = "FAQ"       

    question = models.CharField(max_length=256)
    answer = models.CharField(max_length=1024)
    
    def __str__(self):
        return self.question[:100]

