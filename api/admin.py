from django.contrib import admin

from .models import FAQ, Category, Order, Product, TelegramUser


@admin.register(TelegramUser)
class TelegramUserAdmin(admin.ModelAdmin):
    pass


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_filter = ["parent"]


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    pass


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    search_fields = ("user__tg_id", "user__name", "id")
    list_filter = ["user", "status"]


@admin.register(FAQ)
class FAQAdmin(admin.ModelAdmin):
    pass
