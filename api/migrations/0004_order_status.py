# Generated by Django 3.2.20 on 2023-07-12 12:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_alter_order_options'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='status',
            field=models.CharField(choices=[('pending', 'Ожидает оплаты'), ('paid', 'Оплачен')], default='pending', max_length=7, verbose_name='Статус заказа'),
        ),
    ]
