from django.urls import include, path
from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r'tgusers', views.TelegramUserViewSet, basename="tguser")
router.register(r'categories', views.CategoryViewSet, basename="category")
router.register(r'products', views.ProductViewSet, basename="product")
router.register(r'orders', views.OrderViewSet, basename="order")
router.register(r'faq', views.FAQViewSet, basename="faq")

urlpatterns = [
    path('', include(router.urls)),
]
